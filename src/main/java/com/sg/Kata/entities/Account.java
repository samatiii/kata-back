package com.sg.Kata.entities;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "Account")
@Data
@NoArgsConstructor
public class Account {
    @Id
    private String accountId;
    private Date creattionDate;
    private double balance;
    @ManyToOne
    @JoinColumn(name = "CLIENT_CODE")
    private Client client;
    @OneToMany(mappedBy = "account")
    private Collection<Operation> operations;

    public Account(String accountId, Date creattionDate, double balance, Client client) {
        this.accountId = accountId;
        this.creattionDate = creattionDate;
        this.balance = balance;
        this.client = client;
    }


}
