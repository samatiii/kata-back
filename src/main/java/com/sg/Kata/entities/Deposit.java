package com.sg.Kata.entities;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
@DiscriminatorValue("Deposit")
public class Deposit extends Operation {

    public Deposit(){
        super();
    }

    public Deposit(Date operationDate, double amount,Account account){
        super(operationDate,amount,account);
    }
}
