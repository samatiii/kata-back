package com.sg.Kata.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Operation")
@Data
@NoArgsConstructor
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "OP_TYPE",discriminatorType = DiscriminatorType.STRING)
public abstract class Operation {

    @Id
    @GeneratedValue
    private Long OpId;
    private Date operationDate;
    private double amount;
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_CODE")
    private Account account;

    public Operation(Date operationDate, double amount, Account account) {
        this.operationDate = operationDate;
        this.amount = amount;
        this.account = account;
    }
}
