package com.sg.Kata.entities;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
@DiscriminatorValue("Withdrawal")
public class Withdrawal extends Operation {

    public Withdrawal(){
        super();
    }

    public Withdrawal(Date operationDate, double amount, Account account){
        super(operationDate,amount,account);
    }
}
