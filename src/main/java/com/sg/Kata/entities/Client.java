package com.sg.Kata.entities;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "Client")
@NoArgsConstructor
@Data
public class Client {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String email;
    @OneToMany(mappedBy = "client")
    private Collection<Account> accounts;

    public Client( String name, String email) {
        this.name = name;
        this.email = email;
    }
}

