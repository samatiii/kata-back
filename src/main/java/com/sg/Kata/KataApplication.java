package com.sg.Kata;

import com.sg.Kata.entities.*;
import com.sg.Kata.repositories.AccountRepository;
import com.sg.Kata.repositories.ClientRepository;
import com.sg.Kata.repositories.OperationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

@SpringBootApplication
public class KataApplication implements CommandLineRunner {

	@Autowired
	private ClientRepository clientRepository;

	@Autowired
	private OperationRepository operationRepository;
	@Autowired
	private AccountRepository accountRepository;


	public static void main(String[] args) {
		SpringApplication.run(KataApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		Client client = new Client("abdell","abdell@gmail.com");
		clientRepository.save(client);
		Account account = new Account("C1",new Date(),2000,client);
		accountRepository.save(account);
		operationRepository.save(new Deposit(new Date(),1000,account));
		operationRepository.save(new Deposit(new Date(),2000,account));
		operationRepository.save(new Deposit(new Date(),4000,account));
		operationRepository.save(new Withdrawal(new Date(),500,account));
		operationRepository.save(new Withdrawal(new Date(),1000,account));
		operationRepository.save(new Withdrawal(new Date(),4000,account));
		//operationRepository.save(new Withdrawal(new Date(),2000,account));
	}

}
